import { createStore } from "vuex";

export const initialState = () => {
  return {
    newsList: [],
    pageSize: 8,
    searchOptions: {
      country: "US",
      category: "",
      keyword: "",
    },
    detailItem: {},
    bookmarked: [],
  };
};

const store = createStore({
  state: initialState(),
  mutations: {
    SET_NEWSLIST(state, newsList) {
      state.newsList = newsList;
    },
    SET_SEARCHOPT(state, searchOpt) {
      state.searchOptions = searchOpt;
    },
    SET_PAGE_SIZE(state, pageSize) {
      state.pageSize = pageSize;
    },
    SET_DETAIL(state, detailItem) {
      state.detailItem = detailItem;
    },
    SET_BOOKMARK(state, bookmarked) {
      const index = state.bookmarked.findIndex(
        (x) => x.title === bookmarked.title
      );
      if (index === -1) state.bookmarked.push(bookmarked);
      else state.bookmarked.splice(index, 1);
    },
  },
  actions: {
    getNews({ commit, getters }) {
      let baseURL = "https://newsapi.org/v2/top-headlines?";
      if (getters.getFilters.country) {
        baseURL = baseURL + `country=${getters.getFilters.country}&`;
      }
      if (getters.getFilters.category) {
        baseURL = baseURL + `category=${getters.getFilters.category}&`;
      }
      if (getters.getFilters.keyword) {
        baseURL = baseURL + `q=${getters.getFilters.keyword}&`;
      }
      baseURL =
        baseURL +
        `pageSize=${getters.getPageSize}&apiKey=ffd7cbfdefc2401c9218f1d4c1fdb5ae`;
      fetch(`${baseURL}`)
        .then((response) => response.json())
        .then((data) => {
          commit("SET_NEWSLIST", data.articles);
        });
    },
    searchOptions({ commit }, searchOpt) {
      commit("SET_SEARCHOPT", searchOpt);
    },
    setPageSize({ commit }, pageSize) {
      commit("SET_PAGE_SIZE", pageSize);
    },
    setDetailItem({ commit }, detailItem) {
      commit("SET_DETAIL", detailItem);
    },
    setBookmarked({ commit }, bookmarkItem) {
      commit("SET_BOOKMARK", bookmarkItem);
    },
  },
  getters: {
    getTopNews: (state) => state.newsList,
    getFilters: (state) => state.searchOptions,
    getPageSize: (state) => state.pageSize,
    getDetailItem: (state) => state.detailItem,
    getBookmarked: (state) => state.bookmarked,
  },
});
export default store;
