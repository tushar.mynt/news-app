import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import Input from "./components/Input";

createApp(App)
  .use(router)
  .use(store)
  .component("c-input", Input)
  .mount("#app");
