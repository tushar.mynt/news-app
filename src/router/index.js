import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/News.vue";
import Details from "../views/Details.vue";
import Bookmarks from "../views/Bookmarks.vue";
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/details",
    name: "Details",
    component: Details,
  },
  {
    path: "/bookmarks",
    name: "Bookmarks",
    component: Bookmarks,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
